"use strict";
$(document).ready(function () {
	// $(".main_banner_slider").slick({
	// 	arrows: false,
	// 	fade: true,
	// 	slidesToShow: 1,
	// 	slidesToScroll: 1,
	// });

	// мобидьное меню

	$("body").on("click", ".mob-menu-btn", function (e) {
		e.preventDefault();
		if ($(".mobile-menu").hasClass("mobile-menu-active")) {
			$(".mobile-menu").removeClass("mobile-menu-active");
			$(".mob-menu-btn").removeClass("mob-menu-btn-active");
			setTimeout(() => {
				$(".mobile-menu-wrapper").removeClass("active");
			}, 300);
			$("body").css("overflow", "auto");
		} else {
			$(".mobile-menu-wrapper").addClass("active");
			setTimeout(() => {
				$(".mobile-menu").addClass("mobile-menu-active");
				$(".mob-menu-btn").addClass("mob-menu-btn-active");
			}, 100);
			$("body").css("overflow", "hidden");
		}
	});

	// выбор города
	$(".hdr_city-nav-inner").addClass("hdr_city-nav-inner-active");
	$(".city-nav_current").addClass("city-nav_current-active");
	$("body").on("click", ".city-nav_current", function (e) {
		e.preventDefault();
		if ($(".hdr_city-nav-inner").hasClass("hdr_city-nav-inner-active")) {
			$(".hdr_city-nav-inner").removeClass("hdr_city-nav-inner-active");
			$(".city-nav_current").removeClass("city-nav_current-active");
		} else {
			$(".hdr_city-nav-inner").addClass("hdr_city-nav-inner-active");
			$(".city-nav_current").addClass("city-nav_current-active");
		}
	});
	$("body").on("click", ".city-nav_close-btn", function (e) {
		e.preventDefault();
		if ($(".hdr_city-nav-inner").hasClass("hdr_city-nav-inner-active")) {
			$(".hdr_city-nav-inner").removeClass("hdr_city-nav-inner-active");
			$(".city-nav_current").removeClass("city-nav_current-active");
		}
	});

	// faq коллапсы

	$("body").on("click", ".faq_opening-link", function (e) {
		e.preventDefault();
		let wrapper = $(this).parents(".faq_item_wrapper");
		if (wrapper.hasClass("faq_item_wrapper-active")) {
			wrapper.children(".faq_item_body").slideUp();
			wrapper.removeClass("faq_item_wrapper-active");
		} else {
			wrapper.children(".faq_item_body").slideDown();
			wrapper.addClass("faq_item_wrapper-active");
		}
	});

	// модальное окно
	$(function () {
		$(".tel").mask("+7 (999) 999-99-99");
	});

	//табы отзывов на главной
	$("#nav-tab a").on("click", function (e) {
		e.preventDefault();
		if ($(this).hasClass("active")) {
			$(this).removeClass("active");
			let id = $(this).attr("aria-controls");
			$("div#" + id).removeClass("active show");
		} else {
			$(this).tab("show");
		}
	});

	$("#nav-tab .review_btn").on("shown.bs.tab", function () {
		$("#nav-tab .review_btn").removeClass("active");
		$(this).addClass("active");
	});
	// $('.review_btn').on('click', function(e){
	//     if($(this).hasClass('active')){

	//     $(this).removeClass('active');
	//     let id = $(this).attr('aria-controls');
	//     $('div#'+id).removeClass('active show');
	//     console.log($('#'+id))
	//     }
	// })

	// тултипы

	$(function () {
		$('[data-toggle="tooltip"]').tooltip();
	});
	$(".collapse").collapse();

	// мобильные фильтры

	$("body").on("click", ".fil_open-btn", function () {
		let dispFil = $(".cat_filters-block").css("display");
		if (dispFil == "none") {
			$(".cat_filters-block").fadeIn();
			$("body").addClass("overflow-hidden");
		}
	});

	$("body").on("click", ".cat_fil_mob-head_close-btn", function () {
		let dispFil = $(".cat_filters-block").css("display");
		if (dispFil == "block") {
			$(".cat_filters-block").fadeOut();
			$("body").removeClass("overflow-hidden");
		}
	});

	$("body").on("click", ".mob-fil_set-btn", function () {
		let dispFil = $(".cat_filters-block").css("display");
		if (dispFil == "block") {
			$(".cat_filters-block").fadeOut();
			$("body").removeClass("overflow-hidden");
		}
	});

	// слайдер продукта

	var galleryThumbs = new Swiper(".prod_slider_thumbs", {
		spaceBetween: 8,
		slidesPerView: 5,
		freeMode: true,
		watchSlidesVisibility: true,
		watchSlidesProgress: true,
		breakpoints: {
			576: {
				spaceBetween: 15,
			},
		},
	});
	var galleryTop = new Swiper(".prod_slider_main", {
		spaceBetween: 10,

		thumbs: {
			swiper: galleryThumbs,
		},
	});

	var similarSlider = new Swiper(".prod_similar_slider", {
		spaceBetween: 10,
		slidesPerView: 1,

		navigation: {
			nextEl: ".swiper-button-next",
			prevEl: ".swiper-button-prev",
		},
		pagination: {
			el: ".swiper-pagination",
			clickable: true,
		},
		breakpoints: {
			576: {
				slidesPerView: 2,
			},
			768: {
				slidesPerView: 3,
			},
		},
	});

	// уведомление успешного добавления в корзину

	$("body").on("click", ".modal_recomend_add-btn", function () {
		let success = $(this)
			.parents(".cat_product_wrapper")
			.children(".modal_recomend_success");

		if ($(success).css("display") == "none") {
			$(success).fadeIn(150);
			function hideNoty() {
				$(success).fadeOut(150);
			}
			setTimeout(hideNoty, 2000);
		}
	});
});

// let rellax = new Rellax(".rellax", {
// 	center: true,
// });
